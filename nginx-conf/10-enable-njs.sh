#!/bin/sh -e

sed -i '1i load_module modules/ngx_http_js_module.so;' /etc/nginx/nginx.conf

# Allows to remove NGINX version header, but the binary is not compatible right now:
# sed -i '1i load_module modules/ngx_http_headers_more_filter_module.so;' /etc/nginx/nginx.conf
