function test_browse_root() {
    req "http://localhost:$PORT/browse"
    match $LAST_RES_BODY 'id="visiting-path">/<'
    not_match $LAST_RES_BODY '<a class="basename".*parent</a>'
    match $LAST_RES_BODY '<a class="basename".*<i>📁</i> libA</a>'
    match $LAST_RES_BODY '<a class="basename" href="/browse/libB@1.1.2"><i>📁</i> libB</a>'
    match $LAST_RES_BODY '<a class="basename" href="/browse/libC@3.0.0"><i>📁</i> libC</a>'
    match $LAST_RES_BODY '<a class="version" href="/browse/libC@1.0.0">1.0.0</a>'
    match $LAST_RES_BODY '<a class="version" href="/browse/libC@2.0.0">2.0.0</a>'
    match $LAST_RES_BODY '<a class="version" href="/browse/libC@3.0.0">3.0.0</a>'
}

function test_browse_libB_without_version() {
    req "http://localhost:$PORT/browse/libB"
    match $LAST_RES_BODY 'id="visiting-path">/libB<strong>@1.1.2</strong>'
    match $LAST_RES_BODY '<a class="basename" href="/browse"><i>⬆️</i> parent</a>'
    match $LAST_RES_BODY '<a class="basename" href="/browse/libB@1.1.2/index.js"><i>📄</i> index.js</a>'
}

function test_browse_libB_with_version() {
    req "http://localhost:$PORT/browse/libB@1.0.1"
    match $LAST_RES_BODY 'id="visiting-path">/libB<strong>@1.0.1</strong>'
    match $LAST_RES_BODY '<a class="basename" href="/browse"><i>⬆️</i> parent</a>'
    match $LAST_RES_BODY '<a class="basename" href="/browse/libB@1.0.1/index.js"><i>📄</i> index.js</a>'
}

function test_show_libB_file_content() {
    req "http://localhost:$PORT/browse/libB@1.0.1/index.js"
    match $LAST_RES_BODY 'id="visiting-path">/libB<strong>@1.0.1</strong>/index.js'
    match $LAST_RES_BODY '<a class="basename" href="/browse/libB@1.0.1"><i>⬆️</i> parent</a>'
    match $LAST_RES_BODY '<pre id="file-content">libB-1.0.1'
}
